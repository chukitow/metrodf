package com.mayasource.metrodf;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class StopMapActivity extends FragmentActivity{
	
	
	private GoogleMap myGooglMap;
	private TextView lblName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stop_map_layout);
		setUpIfNeed();
		lblName = (TextView)findViewById(R.id.lblName);
		Bundle data = getIntent().getExtras();
		lblName.setText(data.getString("name"));
		
		
		
		/*data for the marker*/
		double lat  = Double.parseDouble(data.getString("lat"));
		double lon  = Double.parseDouble(data.getString("lon"));
		LatLng position = new LatLng(lat, lon);
		
		
		if(myGooglMap != null)
		{
			myGooglMap.addMarker(new MarkerOptions().position(position));
			CameraUpdate center=
			        CameraUpdateFactory.newLatLng(position);
			    CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

			    myGooglMap.moveCamera(center);
			    myGooglMap.animateCamera(zoom);
		}
		
		
	}
	private void setUpIfNeed() {
		 
         if(myGooglMap == null)
         {	
           
             
            SupportMapFragment mapFrag=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
            myGooglMap =  mapFrag.getMap();
         }
		
	}
	
	
	
	
	
	
	

}
