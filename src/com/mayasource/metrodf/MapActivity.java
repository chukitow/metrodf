package com.mayasource.metrodf;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mayasource.metrodf.db.DataBase;
import com.mayasource.metrodf.models.Line;

public class MapActivity extends FragmentActivity{
	
	private GoogleMap myGooglMap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_layout);
		setUpMap();
		
		/*Put all the markers*/
		if(myGooglMap!=null)
		{
			DataBase metrodf = new DataBase(this, "METRODF",  null, 1);
			SQLiteDatabase db = metrodf.getWritableDatabase();
			Cursor lineasCursor = db.rawQuery("SELECT * FROM lines", null);
			List<Line> lineas = new ArrayList<Line>();
			
			List<LatLng> positions = new ArrayList<LatLng>();
			List<LatLng> bounds = new ArrayList<LatLng>();
			
			/**/
			if(lineasCursor.moveToFirst())
			{
				
				do
				{
					Line auxLine = new Line();
					auxLine.setRoute_id(lineasCursor.getString(0));
					auxLine.setRoute_color(lineasCursor.getString(3));
					lineas.add(auxLine);
				}while(lineasCursor.moveToNext());
		
			}
			lineasCursor.close();
			
			for(Line aux:lineas)
			{
				Cursor stopsCursor = db.rawQuery("SELECT * FROM stops where route_id = '"+aux.getRoute_id()+"'", null);
				if(stopsCursor.moveToFirst())
				{
					do
					{
						
						LatLng position = new LatLng(Double.parseDouble(stopsCursor.getString(2)),Double.parseDouble(stopsCursor.getString(2)));
						positions.add(position);
						bounds.add(position);
					}while(stopsCursor.moveToNext());
					
					for(int i=0; i<positions.size()-1;i++)
					{
						myGooglMap.addPolyline(new PolylineOptions()
					     .add( positions.get(i),positions.get(i + 1) )
					     .width(5)
					     .color(Color.parseColor("#" + aux.getRoute_color())));

					}
					
				}
				positions.clear();
				stopsCursor.close();
			}
			db.close();
			
//			LatLngBounds.Builder builder = new LatLngBounds.Builder();
//			for(LatLng aux: bounds)
//			{
//				builder.include(aux);
//			}
			
//			LatLngBounds circle = builder.build();
//			DisplayMetrics displaymetrics = new DisplayMetrics();
//			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//			int height = displaymetrics.heightPixels;
//			int width = displaymetrics.widthPixels;
//			    myGooglMap.moveCamera(CameraUpdateFactory.newLatLngBounds(circle,width,height, 10));


		}
		
	}
	private void setUpMap() {
		
		 SupportMapFragment mapFrag=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
         myGooglMap =  mapFrag.getMap();
		
	}
}
