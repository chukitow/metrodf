package com.mayasource.metrodf.parsers;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mayasource.metrodf.models.Stop;

public class StopParser {
	
	public Stop[] parseJsonToStops(String json)
		{
			Stop[] stops = null;
			Gson gson = new Gson();
			JsonParser parser = new JsonParser();
			
			JsonElement stopsJson =  parser.parse(json);
			JsonObject aux = stopsJson.getAsJsonObject();
			stopsJson = aux.get("stops");
			
			stops = gson.fromJson(stopsJson, Stop[].class);
			
			
			return stops;
		}

}
