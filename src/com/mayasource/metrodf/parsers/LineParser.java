package com.mayasource.metrodf.parsers;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mayasource.metrodf.models.Line;

public class LineParser {
	
	public LineParser() {
	}
	
	public Line[] parseJsonToLines(String json)
	{
		Line[] lineas = null;
		Gson gson = new Gson();
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(json);
		JsonObject aux = element.getAsJsonObject();
		element = aux.get("routes");		
		lineas = gson.fromJson(element, Line[].class);
		return lineas;
	}

}
