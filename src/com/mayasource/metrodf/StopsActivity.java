package com.mayasource.metrodf;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.mayasource.metrodf.adapters.StopsAdapter;
import com.mayasource.metrodf.constans.Constans;
import com.mayasource.metrodf.db.DataBase;
import com.mayasource.metrodf.models.Stop;
import com.mayasource.metrodf.parsers.StopParser;

public class StopsActivity extends Activity{

	private TextView lblLineName;
	private List<Stop> stops = null;
	private StopsAdapter adapter = null;
	private ListView lstStops = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stops_layout);
		lblLineName = (TextView)findViewById(R.id.lblLineName);
		lstStops = (ListView)findViewById(R.id.lstStops);
		setTitle("Paradas");
		stops = new ArrayList<Stop>();
		adapter = new StopsAdapter(this);
		
		Bundle bundle = getIntent().getExtras();
		String ROUTE_ID = bundle.getString("route_id");
		lblLineName.setText(bundle.getString("route_long_name"));
		lblLineName.setTextColor(Color.parseColor("#"+bundle.getString("route_color")));
		
		
		/*GET STOPS FORM DATABASE*/
		DataBase metroDF = new DataBase(this, "METRODF", null, 1);
		SQLiteDatabase db = metroDF.getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM stops where route_id = '"+ROUTE_ID+"'", null);
		if(cursor.moveToFirst())
		{

			do
			{
				Stop stop = new Stop();
				stop.setStop_id(cursor.getString(0));
				stop.setStop_name(cursor.getString(1));
				stop.setStop_lat(cursor.getString(2));
				stop.setStop_lon(cursor.getString(3));
				stops.add(stop);
				
			}while(cursor.moveToNext());
			cursor.close();
			db.close();
			for(int i=0; i<stops.size(); i++)
			{	
				adapter.add(stops.get(i));
				
			}
			Log.i("valor de i",String.valueOf(stops.size()));
			
			lstStops.setAdapter(adapter);
		}
		
				
		lstStops.setOnItemClickListener( new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				
				Stop itemGotIt = stops.get(position);
				Bundle data = new Bundle();
				data.putString("lat", itemGotIt.getStop_lat());
				data.putString("lon", itemGotIt.getStop_lon());
				data.putString("name", itemGotIt.getStop_name());
				Intent startStopMapActivity = new Intent(StopsActivity.this,StopMapActivity.class);
				startStopMapActivity.putExtras(data);
				startActivity(startStopMapActivity);
				
			}
		});
		
	}
}
