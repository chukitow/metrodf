package com.mayasource.metrodf.tasks;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.mayasource.metrodf.constans.Constans;
import com.mayasource.metrodf.db.DataBase;
import com.mayasource.metrodf.models.Line;
import com.mayasource.metrodf.models.Stop;
import com.mayasource.metrodf.parsers.LineParser;
import com.mayasource.metrodf.parsers.StopParser;

public class LoadDB extends AsyncTask<Void, Void, Void>{
	
	
	private Activity context;
	private ProgressDialog progressDialog;
	
	
	
	public LoadDB(Activity context) {
		this.context = context;
		progressDialog = new ProgressDialog(context);

	}
	
	@Override
	protected void onPreExecute() {
		progressDialog.setTitle("Descargando datos...");
		progressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		/*CHECK THE DATABASE */
		DataBase metroDF = new DataBase(context, "METRODF", null, 1);
		SQLiteDatabase db = metroDF.getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM lines", null);
		
		
		if(!cursor.moveToFirst())
		{
			try
			{
				
				String jsonLines = getDataFromURL(Constans.APILINES);
				Line[] lines = new LineParser().parseJsonToLines(jsonLines);
				String jsonStops = null;
				int j=1;
				for(Line auxLine : lines)
				{
					jsonStops = getDataFromURL(Constans.APISTOPS + auxLine.getRoute_id());
					Stop[] stops = new StopParser().parseJsonToStops(jsonStops);
				
					Log.i("Linea",auxLine.getRoute_long_name());
					try
					{
						db.execSQL("INSERT INTO lines(route_id,route_short_name,route_long_name,route_color)" + 
										" VALUES('"+auxLine.getRoute_id()+"','"+auxLine.getRoute_short_name()+"','"+auxLine.getRoute_long_name()+"','"+auxLine.getRoute_color()+"')");
					
					}
					catch(SQLiteConstraintException e)
					{
						
					}
					
					for(Stop auxStop: stops)
					{
						Log.i("parada",auxStop.getStop_name());	
						Log.i("route_id",auxLine.getRoute_id());	
						try
						{
							db.execSQL("INSERT INTO stops( stop_id , stop_name , stop_lat , stop_lon , route_id)" +
									" VALUES( '"+auxStop.getStop_id()+"' , '"+auxStop.getStop_name()+"' , '"+auxStop.getStop_lat()+"' , '"+auxStop.getStop_lon()+"' , '"+auxLine.getRoute_id()+"' )");
						}catch(SQLiteConstraintException e)
						{
							
						}
					}
				}
				
			}catch(NullPointerException e)
			{
				Log.i("null ponter","null");
			}
		}
		cursor.close();
		db.close();
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		progressDialog.dismiss();
	}
	
	
	private String getDataFromURL(String url)
	{
		String answer = null;
		try
		{
			HttpClient client = new DefaultHttpClient();
			HttpGet GET = new HttpGet(url);
			HttpResponse res = client.execute(GET);
			HttpEntity ent = res.getEntity();
			answer = EntityUtils.toString(ent);
			
		}catch(ClientProtocolException e)
		{
			Log.i("Excepcion lanzda","ClientProtocolException");
		}
		catch(IOException e)
		{
			Log.i("Excepcion lanzda","IOException");
		}
		
		return answer;
	}
	

	
	


}
