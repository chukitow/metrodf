package com.mayasource.metrodf;


import java.util.ArrayList;

import com.mayasource.metrodf.adapters.LinesAdapter;
import com.mayasource.metrodf.constans.Constans;
import com.mayasource.metrodf.db.DataBase;
import com.mayasource.metrodf.models.Line;
import com.mayasource.metrodf.parsers.LineParser;
import com.mayasource.metrodf.models.Line;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;


public class LinesActivity extends Activity{

	private String jsonLines = null;
	private ArrayList<Line> lineas = null;
	private LinesAdapter adapter;
	private ListView lstLines;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lines_layout);
		lstLines = (ListView)findViewById(R.id.lstLines);
		setTitle("Lineas");
		
		/*Populate the lines from DB*/
		DataBase metroDF = new DataBase(this, "METRODF", null, 1);
		SQLiteDatabase db = metroDF.getWritableDatabase();

		String selectQuery = "SELECT  * FROM lines";		 
	    Cursor cursor = db.rawQuery(selectQuery, null);
	    
	    if(cursor.moveToFirst())
	    {
	    	lineas = new ArrayList<Line>();
	    	adapter = new LinesAdapter(this);
	    	do
	    	{
	    		Line aux = new Line();
	    		aux.setRoute_id(cursor.getString(0));
	    		aux.setRoute_short_name(cursor.getString(1));
	    		aux.setRoute_long_name(cursor.getString(2));
	    		aux.setRoute_color(cursor.getString(3));
	    		lineas.add(aux);
	    		
	    		
	    	}while(cursor.moveToNext());
	    	
	    	cursor.close();
	    	db.close();
	    	for(int i=0; i<lineas.size();i++)
	    	{
	    		adapter.add(lineas.get(i));
	    	}
	    	
	    	lstLines.setAdapter(adapter);
	    }
	    else
	    {
	    	Log.i("Vacio","No hay lineas");
	    }
		
		/*ON ITEM CLICK*/
		
		lstLines.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				

				Line itemGotIt =  lineas.get(position);
				Bundle data = new Bundle();
				data.putString("route_id", itemGotIt.getRoute_id());
				data.putString("route_long_name", itemGotIt.getRoute_long_name());
				data.putString("route_color", itemGotIt.getRoute_color());
				
				Intent StopsActivity = new Intent(LinesActivity.this,StopsActivity.class);
				StopsActivity.putExtras(data);
				startActivity(StopsActivity);
				
			}
		});
	}
}
