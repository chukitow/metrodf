package com.mayasource.metrodf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class IndexActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index_layout);
		
		new Thread(){
			
			@Override
			public void run() {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) 
				{
				}
				finally
				{
					Intent i = new Intent(IndexActivity.this,SectionsActivity.class);
					startActivity(i);
				}
			}
		}.start();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

}
