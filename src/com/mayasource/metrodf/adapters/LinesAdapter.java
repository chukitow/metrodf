package com.mayasource.metrodf.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mayasource.metrodf.R;
import com.mayasource.metrodf.models.Line;
import com.mayasource.metrodf.wrappers.LineWrapper;

public class LinesAdapter extends ArrayAdapter<Line>{
	
	Activity context;
	
	public LinesAdapter(Activity context) {
		super(context, 0,0);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View rowView = convertView;
		Line line = getItem(position);
		LineWrapper wrapper;
		
		if(rowView == null)
		{
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.line_item, null);
			wrapper = new LineWrapper(rowView);
			rowView.setTag(wrapper);
		}
		else
		{
			wrapper = new LineWrapper(rowView);
		}
		
		wrapper.getRail().setBackgroundColor(Color.parseColor("#"+line.getRoute_color()));

		wrapper.getLblLineNumber().setText("LINEA "+line.getRoute_short_name());
		wrapper.getLblLineNumber().setTextColor(Color.parseColor("#"+line.getRoute_color()));
		
		wrapper.getLblName().setText(line.getRoute_long_name());
		
		return rowView;
	}

}
