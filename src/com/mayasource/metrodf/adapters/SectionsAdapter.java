package com.mayasource.metrodf.adapters;

import com.mayasource.metrodf.R;
import com.mayasource.metrodf.constans.Constans;
import com.mayasource.metrodf.wrappers.SectionsWrapper;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class SectionsAdapter extends ArrayAdapter<String> {
	
	private Activity context;
	private String[] sections;
	private int[] icons;
	
	
	public SectionsAdapter(Activity context, String[] sections,int[] icons) {
		super(context, R.layout.section_item,sections);
		this.context = context;
		this.sections = sections;
		this.icons = icons;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;
		SectionsWrapper wrapper;
		
		if(rowView == null)
		{
			LayoutInflater infalter = context.getLayoutInflater();
			rowView = infalter.inflate(R.layout.section_item, null);
			wrapper = new SectionsWrapper(rowView);
			rowView.setTag(wrapper);
		}
		else
		{
			wrapper = new SectionsWrapper(rowView);
		}
		
		wrapper.getLblSectionName().setText(sections[position]);
		wrapper.getImgIco().setImageResource(icons[position]);
		return rowView;
	}
	

}
