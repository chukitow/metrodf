package com.mayasource.metrodf.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mayasource.metrodf.R;
import com.mayasource.metrodf.models.Stop;
import com.mayasource.metrodf.wrappers.StopsWrapper;

public class StopsAdapter extends ArrayAdapter<Stop>{
	
	Activity context;
	public StopsAdapter(Activity context) {
		super(context, 0,0);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = convertView;
		Stop item = getItem(position);
		StopsWrapper wrapper;
		
		if(rowView == null)
		{
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.stop_item, null);
			wrapper = new StopsWrapper(rowView);
			rowView.setTag(wrapper);
			
		}
		else
		{
			wrapper = new StopsWrapper(rowView);
		}
		
		wrapper.getLblStopName().setText(item.getStop_name());
		
		return rowView;
	}

}
