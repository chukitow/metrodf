package com.mayasource.metrodf.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper{

	String linesTable = "CREATE TABLE IF NOT EXISTS lines ("+
			"`route_id` varchar(150) NOT NULL,"+
			"`route_short_name` varchar(150) NOT NULL,"+
			"`route_long_name` varchar(150) NOT NULL,"+
			"`route_color` varchar(150) NOT NULL,"+
			"PRIMARY KEY (`route_id`)"+
		") ";
	
	String stopsTable = "CREATE TABLE IF NOT EXISTS stops ("+
			"`stop_id` varchar(150) NOT NULL,"+
			"`stop_name` varchar(150) NOT NULL,"+
			"`stop_lat` varchar(150) NOT NULL,"+
			"`stop_lon` varchar(150) NOT NULL,"+
			"`route_id` varchar(150) NOT NULL"+
			//"PRIMARY KEY (`stop_id`)"+
		") ";
	
	
	
	public DataBase(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		 db.execSQL(linesTable);
		 db.execSQL(stopsTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		
	}

}
