package com.mayasource.metrodf;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.internal.cu;
import com.google.android.gms.internal.db;
import com.mayasource.metrodf.adapters.SectionsAdapter;
import com.mayasource.metrodf.constans.Constans;
import com.mayasource.metrodf.db.DataBase;
import com.mayasource.metrodf.tasks.LoadDB;



public class SectionsActivity extends Activity {
	
	/*
	 *SETING UP THE PROPERTIES 
	 */
	private ListView lstSections = null;
	private SectionsAdapter adapter = null;
	private String[] sections = Constans.SECTIONS;
	private int [] sections_ico = Constans.SECTIONS_ICONS;
	private LoadDB loadDB = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sections_layout);
		setTitle("Sistema de Transporte Colectivo-Metro");
		
		lstSections = (ListView)findViewById(R.id.lstSections);
		adapter = new SectionsAdapter(this,sections,sections_ico);
		lstSections.setAdapter(adapter);
		
		
		loadDB = new LoadDB(this);
		loadDB.execute();
		
		
		/*LIST ITEM CLICK*/
		lstSections.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) {
			
				switch(position)
				{
					//Lines
					case 0:
						Intent LinesActivity = new Intent(SectionsActivity.this,LinesActivity.class);
						startActivity(LinesActivity);
						break;
					
					//Map
					case 1:
						Intent MapActivity = new Intent(SectionsActivity.this,MapActivity.class);
						startActivity(MapActivity);
						break;
				}
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.index, menu);
		return true;
	}

}
