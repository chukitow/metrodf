package com.mayasource.metrodf.wrappers;

import com.mayasource.metrodf.R;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LineWrapper {
	
	private View context;
	private Button rail;
	private TextView lblLineNumber;
	private TextView lblName;
	
	
	public LineWrapper(View view) {
		this.context = view;
	}
	
	public Button getRail() {
		if(rail == null)
			rail = (Button)context.findViewById(R.id.btnRail);
		return rail;
	}
	
	public TextView getLblLineNumber() {
		if(lblLineNumber == null)
			lblLineNumber = (TextView)context.findViewById(R.id.lblLine);
		return lblLineNumber;
	}
	
	public TextView getLblName() {
		if(lblName == null)
			lblName = (TextView)context.findViewById(R.id.lblName);
		return lblName;
	}

}
