package com.mayasource.metrodf.wrappers;

import com.mayasource.metrodf.R;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SectionsWrapper {
	
	private View context;
	private TextView lblSectionName;
	private ImageView imgIco;
	
	public SectionsWrapper(View view) {
		this.context = view;
	}
	
	public TextView getLblSectionName() {
		if(lblSectionName == null)
			lblSectionName = (TextView)context.findViewById(R.id.lblSectionName);
		return lblSectionName;
	}
	
	public ImageView getImgIco() {
		if(imgIco == null)
			imgIco = (ImageView)context.findViewById(R.id.imgIcon);
		return imgIco;
	}

}
