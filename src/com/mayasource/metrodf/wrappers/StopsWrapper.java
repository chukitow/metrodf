package com.mayasource.metrodf.wrappers;

import com.mayasource.metrodf.R;

import android.view.View;
import android.widget.TextView;

public class StopsWrapper {
	
	View context;
	private TextView lblStopName;
	
	
	public StopsWrapper(View view) {
		this.context = view;
	}
	
	public TextView getLblStopName() {
		if(lblStopName == null)
			lblStopName = (TextView)context.findViewById(R.id.lblStopName);
		return lblStopName;
	}

}
